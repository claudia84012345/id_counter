# build
FROM golang:1.20-alpine3.17 as builder
WORKDIR /app
COPY . .
RUN go build ./cmd/id_counter

# runtime
FROM alpine:3.17.2
ENV PORT=3000
WORKDIR /app
COPY --from=builder /app/id_counter ./
EXPOSE ${PORT}
ENTRYPOINT [ "/app/id_counter" ]
CMD [ "" ]