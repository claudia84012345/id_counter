# devops-techtask

## Overview

Hopefully this tech task allows you to strut your stuff as much as you decide to!

This exercise should take you no more than a weekend. If you need any clarification, please don’t hesitate to ask us.

## Task details

This repository contains a simple golang service which serves a single endpoint: `/:id`. For each call to this endpoint, the service increments a counter, and returns the number of times the endpoint has been called for the given ID. State for the application is stored in and retrieved from redis, where the data is stored simply with `id` as the key, and the count as the value.

We'd like to deploy this application, and all its dependencies (in this case, redis) on kubernetes. It should be accessible from outside the cluster via HTTP, and should be deployed with high-availability in mind. You are highly encouraged to use any infrastructure automation or deployment tools that you are familiar with to do this.

The entire solution should be able to run on an fresh ubuntu VM.

You may modify the application code as you see fit, but ensure that the functionality remains the same.

In your solution, include a readme containing the necessary steps to set up the environment, as well as to build, package and deploy the application. Also detail and explain your chosen architecture, as well as what tools were used in the deployment process.

## Requirements

- Simple to build and run
- Application is able to be deployed on kubernetes and can be accessed from outside the cluster
- Application must be able to survive the failure of one or more instances while staying highly-available
- Redis must be password protected
- Readme documents how to run the code and provides explanation regarding the implementation

## Implementation Process

1. Created kubernetes cluster using kind locally with the following command.

    ```sh
    kind create cluster
    ```

2. Created Dockerfile, build, push and run docker image locally with the following commands.

    ```sh
    docker buildx build -t gcr.io/<project-id>/id_counter:0.0.1 --platform=linux/amd64 .
    docker push gcr.io/<project-id>/id_counter:0.0.1
    ```

3. However, I confronted with the  `segment 'id' conflicts with existing wildcard 'ping'` error so I modified line 43 of the `main.go` file with the following change.

    ```go
    - router.GET("/:id", func(c *gin.Context) {
    + router.GET("/id/:id", func(c *gin.Context) {
    ```

4. Created the `cmd/id_counter` folder and move the `main.go` file under it to follow golang best pratice.

5. Created the `docker-compose.yaml` file for local development. You can start application and Redis with the following command.

    ```sh
    docker-compose up
    ```

    now, application is ready and dockerlized 🎉

6. Created two environments for dev and prod.

    ```sh
    kubectl create ns dev
    kubectl create ns prod
    ```

7. Created GCP service account and kubernetes secrets for kubernetes to pull images from GCR.

    ```sh
    kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat ~/Desktop/devops/tidy-alchemy-377317-a5d7edf4dd7c.json)" --docker-email=devops-task@tidy-alchemy-377317.iam.gserviceaccount.com -n dev
    kubectl create secret docker-registry gcr-json-key --docker-server=gcr.io --docker-username=_json_key --docker-password="$(cat ~/Desktop/devops/tidy-alchemy-377317-a5d7edf4dd7c.json)" --docker-email=devops-task@tidy-alchemy-377317.iam.gserviceaccount.com -n prod
    ```

8. Created kubernetes deployments for `dev` and `prod` environments, and used `podAntiAffinity.preferredDuringSchedulingIgnoredDuringExecution` for HA.
Also, added the health check `readinessProbe` and `livenessProbe` to reflect the service status.

    ```sh
    cat k8s/dev/id_counter/deployment.yaml
    cat k8s/prod/id_counter/deployment.yaml
    ```

9. Configured network accessed from outside the cluster with NodePort service.
    I created two kubenretes services for each enviornments.

    ```sh
    cat k8s/dev/id_counter/service.yaml
    cat k8s/prod/id_counter/service.yaml
    ```

    For this task, I used NodePort and assume we can accept access the service with kubernetes nodes IP.

    If we want to control traffic precisely, we can use Nginx Ingress Controller, Istio or Traefik, etc.


10. Set up ArgoCD with the following commands to handle CD with GitOps manner.

    ```sh
    kubectl create namespace argocd
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
    ```

11. Created ArgoCD Applications for each environment.

    ![set up argocd app](images/set_up_argocd_app.png "ArgoCD")

12. Built CI/CD pipeline with Gitlab.
You can deploy to dev by pushing commit to `main` branch.
You can deploy to prod by adding a tag (eg. `0.0.1`).

    ![gitlab cicd](images/gitlab_cicd.png "Gitlab_CICD")

13. Set up sealed secret controller by using helm chart with Argo CD.

    ```sh
    kubectl create ns kubeseal
    kubectl apply -f k8s/argocd/sealed-secret.yaml
    ```

    After that, you will see the new application with sealed-secrets on Argo CD.

    Install sealed secret client by using brew.

    ```sh
    brew install kubeseal
    ```

14. Created redis using helm chart,and protected password with sealed secrets.

    ```sh
    kubeseal --format=yaml --scope cluster-wide --controller-name=sealed-secrets --controller-namespace=kubeseal < k8s/dev/redis/secret.yaml | tee k8s/dev/redis/redis-sealed-secret.yaml
    kubectl apply k8s/dev/redis/redis-sealed-secret.yaml
    kubeseal --format=yaml --scope cluster-wide --controller-name=sealed-secrets --controller-namespace=kubeseal < k8s/prod/redis/secret.yaml | tee k8s/prod/redis/redis-sealed-secret.yaml
    kubectl apply k8s/prod/redis/redis-sealed-secret.yaml
    ```

    After that, you will see the new application with redis on Argo CD

    ![argocd result](images/argocd_results.png "ArgoCD")

15. Added environments and secret to the deployment of application.

    ```yaml
    containers:
      - name: id-counter
        ...
        env:
          - name: PORT
            value: "3000"
          - name: REDIS_HOST
            value: "dev-redis-master"
          - name: REDIS_PORT
            value: "6379"
          - name: REDIS_DB
            value: "0"
        envFrom:
          - secretRef:
              name: redis-secret
    ```

    Verified the result.

    ![app result](images/verify_redis_results.png "Reuslt")